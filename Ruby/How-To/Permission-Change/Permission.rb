#!/usr/bin/ruby

def readFile(file)
  data = ''
  f = File.open(file, "r") 
  f.each_line do |line|
    data += line
  end
  return data
end

# You will specify the file name. In your case the string will be the path for your file
file_name = "/Users/shorbagy14/Desktop/Test/Original.png"
file_time_stamp = File.dirname(file_name) + "/Date.txt"

# This will get you the current date
current_date = Time.now

current_date_integer = current_date.to_i
puts "Current Date Is " + current_date_integer.to_s

if File.file?(file_time_stamp)
	start_date = readFile(file_time_stamp)
	date_integer = start_date.to_i
else
	out_file = File.new(file_time_stamp, "w")
	out_file.puts(current_date.to_i)
	start_date = current_date
	date_integer = current_date_integer
end

puts "File Date Is " + date_integer.to_s

# Getting the difference of current date and modified date
difference_integer = current_date_integer - date_integer
# Converting seconds into days
difference = difference_integer / (24*60*60)
puts difference.to_s + " days"

# Here where you will check if the difference is more than 90 days

if difference > 90 
	# You will put the permission line to change to 000
	File.chmod(000,file_name)
	File.delete(file_time_stamp)
  	puts "Change my permission"
else
	# You will do nothing here
	puts "Leave me alone"
end
