#!/usr/bin/ruby 

require 'mail'

options = { 
            :address              => "smtp.learnitbro.com",
            :port                 => 587,
            :domain               => 'mail.learnitbro.com',
            :user_name            => 'test@learnitbro.com',
            :password             => '12345678',
            :authentication       => :login,
            :enable_starttls_auto => true  
          }

Mail.defaults do
  delivery_method :smtp, options
end

Mail.deliver do
  to 'info@learnitbro.com'
  from 'test@learnitbro.com'
  subject 'Test - Learn It Bro - Ruby'
  body 'This is a test by Learn It Bro'
end